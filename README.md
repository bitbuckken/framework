# README #

### Framework CSS ###

* Ce framework est simple d'utilisation et permet de définir rapidement un style pour vos développements.
* Version 1.0

### Comment l'installer ? ###

* L'installation est très simple : copiez le fichier import.css dans votre projet et utilisez les classes du framework directement dans le html.

### Fonctionnalités implantées ###

Des styles pré-configurés sont définis pour différentes balises ou éléments HTML.

Notamment  : 

* Boutons
* Messages d'alerte
* Page Header
* Jumbotron
* Éléments de typographie